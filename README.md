

Update 06/09/23 11AM:
After experimneting with the homepage code it turns out you don't even need to delete the code on the home page. 
I am now looking into a way to grab the home page html code via code but it is returning a status 302 error, I am going to see if there is a way around that.
```
Request URL:
https://purple-air-dev.myshopify.com/
Request Method:
GET
Status Code:
302
Referrer Policy:
strict-origin-when-cross-origin
```
-------------------------------------------------------------------------------
Update 06/08/23 end of day:
Turns out that the index.html is just the direct html code from the shopify homepage but with all of the sections taken out. I am no longer woried about any of the ids or tokens from that page and I am glad that i know where they came from,

I am still worried about the custom.js file in the theme assests folder with a shopify app token being available to the public.


I would like to see what code is necessary to run that file, and to create a way to automate that process. That could make shopify the single source of truth 

-----------------------------------------------------------------------------------------------------
Update 06/08/23: 
Gitlab does not like outside sites accessing its raw files and returns a 302 error,
A workaround has been made by using gitlab pages https://doug38.gitlab.io/universal-navigation-html/ 

This is a basic .gitlab-ci.yml file but as of now, anytime their is a commit to the branch it will update the gitlab page with the newest one, I currently only have the index.html moving into the public folder.

```
pages:
  stage: deploy
  environment: production
  script:
    - ls
    - mkdir .public
    - cp index.html .public
    - rm -rf public
    - mv .public public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

After reviewing some of the files, I believe there is some security risk involved

In the theme assests folder there is a file called custom.js which has an apps token and apikey, this is information is accessable publicly from the store site.

Currently these keys have access to the following items to the store
```

read_analytics, write_assigned_fulfillment_orders, read_assigned_fulfillment_orders, read_customer_events, 
write_custom_pixels, read_custom_pixels, write_customers, read_customers, write_discovery, read_discovery, 
write_draft_orders, read_draft_orders, write_files, read_files, write_fulfillments, read_fulfillments, read_gdpr_data_request, 
write_gift_cards, read_gift_cards, write_inventory, read_inventory, write_locations, read_locations, write_legal_policies, read_legal_policies, 
write_marketing_events, read_marketing_events, write_merchant_managed_fulfillment_orders, read_merchant_managed_fulfillment_orders, 
write_metaobject_definitions, read_metaobject_definitions, write_metaobjects, read_metaobjects, read_online_store_navigation, 
write_online_store_pages, read_online_store_pages, read_orders, read_packing_slip_templates, read_order_edits, 
read_payment_customizations, read_payment_terms, read_pixels, read_price_rules, read_product_feeds, read_product_listings, 
read_products, write_products, write_publications, read_publications, read_purchase_options, read_reports, read_resource_feedbacks, 
read_returns, read_channels, read_script_tags, read_shipping, read_shopify_credit, read_markets, read_shopify_payments_accounts, 
read_shopify_payments_bank_accounts, read_shopify_payments_disputes, read_shopify_payments_payouts, write_content, read_content, 
read_themes, read_third_party_fulfillment_orders, read_translations, read_custom_fulfillment_services, read_delivery_customizations, 
write_payment_customizations, write_themes

```
I am not sure if all these are necessary. I am still not sure what the app even does besides supplying a token amd apikey to give access to those items.


The index.html has some lines that require some access tokens and ids as well  
lines 29,43,46,47  
I am not sure if these pose any security risk at the moment but I do need to find the correct tokens and ids to change them with, I am not sure where these were obtained.  


There are a lot of files that call the cdn directly, this is not good because it could be a day or more for the any direct links to recive the update.


```
Most asset URLs are rendered using the domain cdn.shopify.com. In certain cases, such as images or stylesheets loaded on a
 storefront, assets are loaded using the storefront domain, in the format {shop}.myshopify.com/cdn.
  This is done to improve performance by maximizing connection reuse in the browser.

Using a CDN means that all of your online store images are cached at thousands of servers around the world.
 When you make changes to your images, Shopify informs the CDN that the images have changed. To do this,
 Shopify uses the asset_url filter, which automatically appends version numbers to all of the URLs that it 
 generates. For example, a version number appended to the end of a url might look like this: ?v=1384022871.

If you link to an image without using the asset_url filter and upload a new version of the same image, 
then the image on your online store might not change to the new version for a day or more.

```



-------------------------------------------------------------------------------


GITHUB REPO: https://raw.githubusercontent.com/vitalikpetreniuk/purple_air_verstka/main/index.html  

GITLAB REPO: https://gitlab.com/doug38/universal-navigation-html/-/raw/main/index.html  

GITLAB SNIPPET: https://gitlab.com/PurpleAir/squarestudio-theme/-/snippets/2550061/raw/main/index.html (This is what we would want to use, I believe everything that needed to be changed was changed)

I have made a public repo in github https://raw.githubusercontent.com/purpleair-dugen/uni-nav/main/index.html (same as the snippet above) and this works.

gitlab links will not work as far as I can tell, You cannot host a html doc in shopifys content files. I will try adding it to the themes assets to see if that works.

shopify themes would not work for what we want, it works if no changes are need but the way shopify does things with the ?v= means any time we had an update all urls would need updating. I think the v is for some sort of version controll
https://cdn.shopify.com/s/files/1/0550/7707/7224/t/17/assets/universal-Index.html?v=1685571400'



the code below is the custom.js from the shopify conent files

```
jQuery(function () {
    $(document).ready(function () {
        $.ajax({
            url: 'LINK GOES HERE',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                const url_to_css = 'https://cdn.shopify.com/s/files/1/0550/7707/7224/files/custom.css?v=1685568151';
                const pa_body = $('body');
                const head = $('head')

                if (pa_params.includeHeader) {
                    pa_body.prepend(`    <header id="purpleair_header">
        <div class="purpleair_header-outer">
            <div class="purpleair_header-inner">
                <div class="pa_header-logo">
                    <a href="https://www2.purpleair.com/">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUYAAABCCAYAAAArMX9SAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAACZ6SURBVHgB7X0JfFTVvf/vLrMlIYR9URERCiRSF6gW3LIBgnslE0A2se/PX+1rfbW1+rQP0bYfa2310WdfoUUFIkkmAioqoiwR2bSgorKDsghCSMg6mcnMvfe8728SMMncCdkZyP3yGWZyz7n3nnvuOb/z249E5xEWpi3s5iTn1ZIkjRAkBkgkDTLI6C2T3AnF/HHi4ydBZUIS5fh9ShLSt6i3XchiR9AIfnXvmnu/JgsWLFhoABJFOUAApZzROX0kXRoPgngzPlfg8FB8HE24hg9fR0AgjxiGsU6X9GyLQFqwYCESop0wSrljcu8DUfw5OMCB4P5caLFMLQCIZBDXOYTrzfNWeRfO/GjmSbJgwYKFWohKwrgueZ16VD061E725/DnLdRGEELsAQf5dGdn56XjV46vIgsWLFigKCSMnmRPHNnpVmGIRyH6Xk1t38YKEMhnHLIj664P7jpGFixY6PCIKsL47rh3HaXB0mkKKQ9TtR6x0e0DcfOhdjF0kDbIy/HUBB0k6ldAtM7GNZ6fuGbiXrJgwUKHRjQRRik7LXsmiOJjaNXABmsKMlDnEPSF20mno9A6HtaFfsQm2coNxVBlXY7VSe8MXeJVkiz9GPUHo769wZuTVGUI41Xc/7kJayZYhhkLFjowooYwwvL8iCzk3+Jn54bqgYAVgID9PugLvhm0B0uVU0rQqTmD7p3uQO16bM1eMXyFq6JTRQzOuUKW5QfxuRXHYxq4dhVE+Jc1Q/uvyfmTC8mCBQsdEuecMEJ8lbLHZN9kEzYPiFbPCNXYMHIY4u5iWKhfdq91H6VmwDPGMxy85gt46utAOU05SBBHIXQxl7rTI+48t04WLFjocDjnhDEnNedy6AWfB0G6k6mSSRU/iOcHEJWfnLR20pdMuKgFyBqXFa9q6sOyIf8b7nYRmfdBFQjoeBDgtWTBgoUOhxb5BLYUbIGWFGkCCOONEYgimEh6W9Mh2q6d/EVLiSJjysopZZqqvYifz+PzTYRqDnCnT2YlZ11MFixY6HA4l4SRzSHXSoY0Cb+7hZUySSTKI5Uevzf/3s+pFcHE0at7F0Kf+L/48xSZNk4aYVNs0zzkUciCBQsdCu0iSs9OnG0f3H1wkiIrA4Qs4mVFjgdHxqTvNhDGNNNoFkEr9aA+a9L6SUeojTB34FxHr0t7PQqO9ekIVb70kz9z2uppu8iCBQsdBm1CGD0jPa4qtSrB5rCNBIGbJktyMp3F2lwPBQYZD2SuzlzeGuJzQ5g3fJ6tS0KXreiJH5oUe2EBf1TuKs+zDDEWLHQctLoonTMm5wopVvqNw+lYKZOcDaJ4JzWNKAZgbMmTNXlTWxNFxqxtszh2+jFwsMUmxbFof6pSpFi6RgsWOhBUaiVwfHOBUnAbrLn/BsKWRk2JPKmLPfi84c53H6d2gmqom6vkqrdBiKfWL8OzDNclnaNwDpEFCxbOV0iQZLsYTuMS0Ki+pJCuCvVoaaD08P0b7y+vX7lVCOMrM15xFhwuuBu6OnbQHtoCAT0Izm2lpElbqB1RUF7ghTj9FpSemXiGuv6NEl0EI03/2cmz1Tn5czSyYMHCeYV5yfO6d1G6sDvgGNg0Oks2qRSqOhUfe5wzzpeTmrNyl7ErG/Pbf/qcFusY5902L6ZTZacHVFn9Jf7sSy3DAV3WUya933YGl0h4ffTrg6BP9ODnVfXLQDD/ZhO2//rJmp8UkQULFs4LcPRbXkreNWD/ZuMPl6ZpL0Eq/Nxhd1RqAU2ulCtjXcI1XFKkOai7OaAE/jB11dT9fG5LcxtKCd6EiSCKv6GzEUWJKvF/KT5MXL6DiFqARpbgIszG8vdngUDg7nNBFBlFoqjAMIxIbkE/rKKqxCXJS7ovT16e8MYdb3SC0YZDC6M+0a8FCx0VuTflDoHI/BIkvr2Kptyn9lBXKz2Uwgqq8DrjnGVljrJvM9dleshL6aA/cQ7D8SK4x2v43BZN7Kz0rB/byb4EPy9roFoRCOgGiMjrZUM+TDodB5E5ju94WK37w/DRyZCMwrLisg0whJTSOQKLyom2xDlgtR8PczaXKICO4zDEY0zMddJP4e8CWZF3oexLLagd2nPjnpNz5swxyIIFC+ccHOFmC9r+jnm6v1AvfK6H2uMq0JlMGISZoQvNUxDMItCljbtX71435JYh/dSg+gTE6wDUaf/ZbMLoyfAo4pTIxkUyIlRhYvIxbpRTGaxcMXP9zHPCCTYFuem5/wHC9wx+xjbqBEGc6uxLiOA7Yb3ephna+vLS8l1s6SYLFiycM0C6m64q6oOGz5g8cdPEA55UzwzQqrkghAtBGA/JQo4HoRyJ+d7N0IxpmfmZO3LSc27G8Sdw/PfNFqWNIiMNF73NtFDAiGKIVbrQH+ul95p/PhBFBkTpQnRaSaNPkMiF/69VJGU6nvlpqBReTOiS8It/jvxnV7JgwcI5g2pTMw3ZeIeJYu3jGmm5masznz+15tQzkA6fwxzuL6nSLewa6Nf8W2RZ3gPimNpcwijhAr+uIQx1AN0h50p8HyLyr3ffuHtLSn7KeWPJlVX5EDqoyXvAsK4Vz9wFP1OwKv2+U1yntXnpedM44ocsWLDQrngp+aU4TMohIijWR6oziyDVBWHzEKRJuhQSre/Lv8+vG/pODvZolrvOktQlw3DBK8w0lKAQ30J/+Ih7nXsPraPzCoePH952Sc9LNkAPwTsRNs+VSZAdfXAlVAjzhvYZemVWQtbcKZumWD6QFjosXkl+xWnX7C5wZoqqqsEd2g5vW7q+9VB69OO8qxCRt9cvU4Wa6knxcHkvoQg3GJnPT4qT80+XK4ryDaTdoc2a/JDd2YqTYFYGjnEpLD176DzEr7/4tXdp8tI/BuWgT5GVKdAd9oLukMm/HuIK0W+NvRaII+9x/e92l30wuMfnM1Zn5FM7gNv5VPJTynfl3zVaf5w+IN1w57mN0OkWLLQi3kp+q3ulWjlFsSvJQhc9cOhUkpq03pPuyXWvdh+mNoCiK5xBiwkvS3F1ItqgYxwFYjgYH96H3iE0sfy68uvObISHNsaDqSlrMmH0JHrsODERcrgjjGOUqAys6Hw6j3FP/j3fesZ6ntUD+leSIg2A3lGBIauKrVl4bg5t7Ayi1yu0XYIMa7yIvGVCaP8ZovFYLHp40jwPu9e4N1MbgvfM8eieMUlG0nWJCYmNJoxGsSFAvIvwfEexKBy3++1HqJCO1s+KbqFhwCBpxzS8FotTMsbLmUVUVOOzSqPyAxbXKMqwdNzSi8Fd3YKxfJFpBUEBn8+3YNqmaQXUBPB4LA+W/wK04hFwYa4z9ELQWBg4Bi64Y8Gv7n8rPOqkpbi0/NITB7oeKFZsCvsk19mmBGP8Wb/h3+IMOruSk2aAQ3zkYNeDnHd1a6iCRJehvV81iTBy/kRN1lJUQx1hlj8RA+LjyfmTz0tusTbcq9ycimzRbJot76SdUh7lhRJIcHIMX4wvJjYY21W36ZdidRkEhjIdRWPRHZEs2dxT16JzfrfwhoUPTN8wvc0226owKmKhxpiO+90Dotzo87iBeHde/DxlN+yFwi4KqA99uOjiRQumvd+0ydCRoZ3U4jHR7sKYeBiE8HvpQiLmxt/sbO/8Mb6jijCyd0mwNHgDbAJ/wDjoYVpJoqDT4WR10BJqAsr95SPQF1PDbBES2bBw3BnnjcvBX62ucBuxbUQwNz33S9x7DObwG3Poezc6jHOtZnE6lpOS8w6Yn1/ouj4Cf2997YbXmMNMhFHms0YbX7JTsq8jGy2BGP0qHizJtJKgj+kCEse4Q08TRYZ7s9s3fc30ognrJ+zLXAPb1prMv2MwzTR0gxNlfEhs9IoEiVJcdteflly3pBe1EZxVTsj9Ujw1D7E49xJ8X43vsWjvHKfhXLZk9JKbyULjIJMSEtFEmMpFxvEYERDRZ4w7ST1kQ74pIlGshg0i6D3raF2TGCkhi+tw3UgeGix9/YjaCJD4/oR735KYlpgaqQ7ex0kQyiAWsU4ZGRmK4lLuweFLAnLgzQYJIytNs9KzhuWk5yyEzm0zXvjt+PCDmneQoP3UgYDBLqAnKZ20btKa4h3F48FBPkqRk01w1slb1E7qI8vSlnWjKAcGFasBrod08Ebe6LxHtg7faiMLFxwM2egL8fLGs9XDeEg8mXLySmoKFPKLyHySYE6U2giT10/+DDaCv2KOPrwkecmQgBFYrWt6mqqpZ6LblI3KCS2g3e4TvryM0oxULBCZ4BYX7dX3Ho5IGKFn6+pSXA/ayAbqKU+LsPVAbZyq0qu2UQfFrO9mVcLo9KJO+gz0VKTQQuYYpuMl3crZiOh8gEQJWFGnH+x28JoaA5SFCwjg6oZgfg89a0UZyhUi5r4aPQZ0oW9CbdMsWRhLxVD7fEJtCG+Vdz6UGIdtqu1ZWZXHyIp8bEf+jsrT5TsSdyh+8hfG2GJuAVPzGNq0DrrQlWwxNyWMnBgC1ppZsMg+DHp4WSPaEMBF35ZdcptYmc4jiEmrJ+VDtObY8S8i1OmJF+Q+phzrR+2IkA6xmqPfXfuD43sxeFmP2FAi3iEYOFOXj1negyxcUABRZB15Y7wt4jBOroIeLoEaib3BvV9hfM03yXVajrGYVSkq25SRun/j/RWKofwBBuF8hZR7OKotMSXxidfTX58JY+j/S+qT9ESsPfaP4BLvxpz8h1Qp/f10usMwroUNDl39XR+E1ehn1IhsObzPMziKFYpQ5u68dmcFraQOj15Gr/UnpBPPybL8JwyAPmEVBN0Etp0HZLtZ8KFc/xTv9M82YSurfTwgBXhxjMUAYt+uqVBYD6fwicI6pglBLchb3J5sjwTCFtoenB8AhPFm0TizAGgLDXY5XEPwu1HeFcx5ZY3Lmu8IOorAPY7i8DuItidxu0+kgPTWfR+1uYVesJfJqjGr/lGilXyqCY0z6VwG1cEgjGcDc6IEz75JM7TNEJ+3z9n8vW9lHcKYQRlKYnrik6j8KzxEXIN3JFGByT0P1PhVb7z30OGrD3utJArVSMlP8WPQ5SZKiYMwEH6FzqprsZaoE6yXj8EiuKC9tkzA+yryab4PJ+ZPLIlQLi2+afE7DtXxH2jzz0xUJ72YY3g1+dVPKD+6LKsWmoeh6tARYGoua7RwLOhyTdJ44Wy02xlvPIevVz3JnjwveVUj3tBnvjWzoj0X17Hvj2WPi/WYk5uGFw+3++1+p91hN8pPlQcP3H3AZ0a3zhBGNtvrp/QJaPDDxGyzGSSqhAx+EErNvKAa/Nu01bVcOd4iC7XAq+XCtIVLnORky9wtJlUu04v1ZHyvoShAaKCup28Wpyxe4JAdN+DQ1fXrgLtI7BLfhY0wIcLIxjnUDdu2Iq40rvyObXdUmt2HCfCK21a4SgIlcVCEn5mSTtUpsHJXsTGrdn3maq60X9nVr/nrTF+n5NR+suYnp5jb2TZ8m1rQs8BVqpXGwcYYD9HI67A5KvuV9Ctj1w2u/9bwt2L8Mf5YzVZXfVRlVFX1p/4VHLo6d9xcR3df904Ocjj9Nn93m2bzKXalzCW7vC6fq7K9wlv5mX+g/aCTM9bpDFQFHOByemjs8MAZnQzZm+BKCFQcqvC30M+U+3Oyqe1A0FZIFwfwvjPrlSRAZzd8wfULOpllvTbDojGLYvGe47j9Dk7qX0GUnZod84rxStV9+feFLdKv3fpaF8lXN1k0v2un3VkxfuX4qtmzZ8v9PusXa6+yx1FVNcMRL8eXH3IcKvn5yp9XNdSWmmgb/nw/NiMovE4TRkkv0a9GRzyIfuocgbUug55pAaw4/3Pvhnu/JgtnxfQ10/fmjs59DT+vQZf2rF8OHS5nJooKwngawib2Y/Hbh7FwVX1jC8SPJMNvsE9aOZe9rr7+AI5lguuIOV2HCWxlQuVqEM1nzAb+X8f91d6nss+tTtn5KBT/Z7a/AFFkJ+j3Xr7x5ednfjTzTLz6EBpyQ9AI/kqRlTo6WUy0Ciw8M7ICWeUOu2M8COI4DuWUVGkg2lCF8m37uu7j5MlbxWwh52zIGQ5Z8I+KpNRZ9GPkmF1FUtGTS8cvDWhV2mSIi6ngihKhcujFGapgyfy2gir2lqvl78EguYLi6Zu24vLZIFekFPVDJ6YZNmOcrutDIVlchL/j7GSH5CfKoOo4UBoo/VruK2/J6pmV82aPN7/Ly8trcns8N3q642uMWRkYnxzczIv3c2dNBNcZ4NjwWGcsq4caRRjtwv7/0ebp9Y/HyrEb8PVg7WOc71SpUl5Cv9cxBuF9gLcr/+u74959u+zDsltkm3wP5tMIoYjLIN2ofuE/0MPfg3f6bJKfZUMIEUa88C4wGHDc4AhTyyNnyxHiZUeV4/nMjZnHyEKj4Qq63vOr/hn4mVa/DIPvRs9IT1f3ZvcpihJMe39apSfdU4LJIaT6XuKCunolb0j/+BQ9JSVS4lV4huvqMx0gUH5wkqZ7/vT29bZhQPPAHxHGrEhUHGOPYSfbM4QRk4oJHYtvvetfC5MuSXbI7EIyC3V6f9/MEKEeLmvyAHxvnf/2fKVzQmcmOCNN7tkJY38szknC76l1HPWrq/bnD55pJAySKcZJY8HcgXPf+/n+hrmTpoLzBxb4CzjLCy+Wo9Gv8TXJSWo1J+SjejWI+9V4yDE2xTZ8QsmExXgP79V2Ym4MDIcxCs/U3aRIQAf3gUoqlx0kXptqAedcjqExKCMj40BjCDI43JvR8GEmRWFSqSqrDgy5G0Fr6urlQX/wWV9WVSbDSPIsfvN7pdPDk0P8wGSwy1GrEcZqscKgYXjgsfgVE1ZDkIYX9IIUlP5w18a7LKLYRNyRf0chBsbGCMU9hFO0mZNrcwBCzRxCnFnkDDiWYxi0ZyYDJqqpdqohtx6s/BKuYV6OC4I7rFMGjini9VD2MNrJXEdvk0YUo+yMNRSExvyegpgz/BnKJzcQvcRt6IT23S6r8p/6XNpnNLUybEFbJkTmZ9BmdjLufFbXKBB01HWjzX8emjz0dmoiIs53om8THAl7sJh/AwK5s34hLzrgGm+c6pvauM3upAi7BAhzzSYWZLP67Dh/G4yZzBX2N7+caNWIO5n1RFTtn5QUoc5aWJT+4v7I3eR0XBaqAXHs3QhFMeDZh1MUQeukXUrVGdnNBuiXQW/QR9ECg27AjIgUxnYSYnFjxiyrBgbj09iIoUHQv/1z8Q2Lm+bs3AByU3LH4jn+gJ8/aKKvKKejHgIu6rfZydkjGnsS5jwvJJzCPzxtoCRWsS7PV+T7FosF++OacYW3B4PBpmyJ3FLwWOS458GRCC0WiE+pFSGz8hxU+i4y8WXCzYrQLS9PWjvJipdtAZatW7YVnek1KVIxuS+i6ICUPSb7EptuY87p2rBSzFiIUF96T3ijJ7GEFHnPIrT1RJW/6gQ1ERwixjpK/DAi31bqZbfbn4BI3dwtgs9gScqSJHCK/wBH1N2sMVSdCR8mi9D40SK050pwz79aNmpZT2oEYimWiUwfkyIdBo3l/IOz0INIsmkibAM4tDURhHEYtS8kiuBcjufXsGB/Tq0IVRUqbyYf7vkOuR6s9PIgBTdafmstA8dbZ4gMVkMMqlek4q32pnYABnMCFN4/AvGr48eo6Ipcs+9OH0yLu9GeWzAfwxZJTJJ9KNsF3Vr0ZtwRIQEtlD4NYtex4u+KG6275ckFEX8zcx68BxEODEK/3IR+uxiXDfP3xfFRvS/tfTvtp9epmZg3fJ4NesJZ+NnTzOCJPt+Ep9mAsv0KvxKJBmJO3oU2Dqq9KKAety9dc2mTYLX9a0Nuc5wBSBSLEdDJ9cLz1r2fEAcqHBVnCAx0qtsxQtnhOYzgwhjGFuv36dxB1CwcbJTaO+XjKWXUilChL7i+Ji62LiQ6jpew7t78e49OoSlkocUII4zodx7cXXmCtPU+MRg9V2EAPSvrcl2uAxMM4ysBE505iNgIHgnsd/ZK0BZknVN0LZKsmJdoH9q9RRbyMV3WWVHvkgxpUxMMJGwRXwvJ6fHMtZmfMSPAmaQgQ/EiwSLuoLAzJOqB+qMXjVm0EgYrLzUDMfExA0CEb0K77SbPtRfXf7S30fuT025CHHyRNDrpY3DDT9QYpGqjG97vdQM2DMgiEy7vNPSTel9FVYbhec243Q1yUD7jylJWXna0S0KXvcwh1l8ccP7Yv4z8i+uXm3/ZrqoVtOMIPh/BAMVug35OzmEYRqtnrFKx7lxvNtRx86OYTPssbrF1gIFUHG7kDemTHL0u6uWkbdSmhBGThkO5rpHM0643BB0N3aTres6U1a27KrcCAmj7UtVQX6nwV2z3VfmKj207pvdP7m8PlAca7cKC93AQ/fJCaWnpl6fHuzvfXTE7efYbiUpiX7y3/zY5zQ4DxiA1oHJGot3UDDhl59UYF31N9IqwcOlzocLaVPsgW549CZ53pGKJ1S8DqTpDzRmg7UNg2eUkLxEJo6qql+Prh/UXQN6SBP8+LP+2/Ayh48U6Ny33I/xkP9y6VmSJLupp78mZ7v9F7QS0+V9QJvxRI+1juUAuyNyZGeCcj0cKjrR6YIkK4mcaC43jhYZsHCULrQMZyn3zJSZw4uiJaI0kYaL4saZrz+y5ec9hyqfogqBPK0Xlr2esnXGManOyTYzMUUh5z6f6ttfn2tkh+LW01962ke0xMtPJSdQHojATqCYTxtBm8CLv8pq9guqWSeKQbuhvm53nznMHcsbmbALnzyJv/ZRwQyH+cmjnfjOGhg2tuPYVuLkZB/wdiObO+ly2ZJNWgRhx7H+Ye43NZruV2o8w7gPxfnpi/sQ6/cKGImoDqKFY3jDXLomXj5PLPljWZOW1BXNAtLvEhDPToa4oiartVqv1dEwQT2AS/bNQL3z+ofyHKqKOKFJoV8e8GetmtGjxxuPquM4n4IaPT6Wp4RWqqFg4xBpwjWH6pFAOQ7l5xrO89Lx49G8fMCDh+kshOW2S7XFwaxVm54L4Mfd/scl44vNunD98/mZIIGGRR3bF3hcPzP5+4YZWSfoiKAXD5rv7PfceT7pnF5nkTUA7R6PshfrRSq2NGnr0oaEbW6idoJquWLDM4YUfrZ2k1ULzEXKJkkytgBq4le+ofcC58TiA3+yd6hh8JWhjYSgdlKBvoPR/R+oubXko76GoHQOYzPuohcDkrpRkqSySyqh7TPdAiV5iSnzRV47aUT9NgV/2O+y63Rkh03pvHJ8V6dyGMgBCyhvaxd6FXfDCCKNTcV4MTtQs96KGh9nu3+c3dW/CwvEGjFlpJg25GAt7Mn69SW0IEXJ8lY6pPdRiaifwasWsaOM2mLfQLNjJztEDZn2sYWC1D1cu6CtwKH/DghfOhWggjIpUEBTBYyACBeAQvRRtRhYTQMSvoJZCIh8WgYgGhNLyUsNwGYVQ9puda8e5TmoGbMKmhs5vZWCh7VahVpimEQtqwWtgTe5hYmArAOHZdd9B82w3Ps23KtYeq1M9ThPX6YZ+udZDnrfd1IbJUCDBgDZWZeZlttsizYSRzfH1049zuEEXstAqsMv29AiOu2xRbZc9cnD/bzHAl5vFL3dwSJqsNZjJXpZk83KDmdbmGSeFLnQYbzRq7s7uka5riASnzWnuBC1JqRFCfg+iD3ZGuua769/9OmN0xjeoN7DO9UiKwf1+6Ep19aa1dEHZI1R01DFOW17vOK8MPZdftTzh7s/vtiZSCxBSeMsiPULxKUmVVpOFcwl27YkoDgcSAjZb0GbqawpC4wcn0yx3Fc7qE6fEVUZwjxKCQ5abiJA6QKaDwi/CfE05Iw7amxLhxHhVV2/LSc1paH8fMyMHbigGVaqVHEVzYRFGiHL7wH6HTVx04uXBhCCb4zeQhWaBVSO5Y3PvwvdgyWShhkJ57cSVEy+4UEuMHScmmqkizBHvkOSgTCJ6JPUY9uOkaqelsEZB/8hMgmm2dZzHRLFZ4vy7+e+WZ6RlFFD1Pev0FcbKO75y34zpn0wvolZCjCOGo9si5Vi9Aov3FXJD7Guk1yXRAKhi2KfybToP1C+NhSwb8kr2YapfwAkswc38iPeRJgvNAqyKA2RdvhccidnEEjje7KiJaADEKPOJIFG/KqoynYQuwxWLxThawiAZMkTlm5akLjENp5N80iUgGMlmZeybGpSDzTKesWET0vQ34PLCLLrQBV+JBeSs4X3s8M2pus5aL3m2Ct3iBGoDcHAIZ31/M/XNRoUjni+QVVK/wIOFGQAwWBKwIqZSL2rMni8W6oH3oMYC/BN8rjOL6UXffh2sDDY6E3K0YQ7NYbGtFM8WHiJoULxdsV9f/zAnGQ0EA5xDbxRFEdCe6/EcVzMBqX2cI5JURb2HjQwRzvtWMZRm74ypKMqXuHa48Y03vjdoGowaEfdi4fGVmJI4QbEpv1uatrTBDE1JlNSfqpMwtA0EDfNq3rNug3I+QdZtejlWvrDMFKykxWowihN31h8wFhoGJ0YVMeLH4DRmRMj+ogMLWju+s53B3OJh/B+eqVsKGSV+9lraayOplpg4dMPQgVhwH8A5gym60A8Mwm+T5CTWlYXaC6IY061rt/vRVvN4WCm0AdzXPt3X7A3gvEHvblznU47TrndtGfgppdH9nK+zdhHPS0+yp7cRa/wUEt1TELt/qknaM540T52+rnNOtY67ufuNnx0S9UVbrriQ6ISasSqjOC8tbzkejhWvdcQfvAR+KY8nKUkf4nsnWWgUctfkDpSd8h/JLDkHhfp1g2yTV9B5DhB+jlFm4h62cxzH8tok23LPaM9WPPBO1OuFSTwKJ/XD39E2gRROYovPck+6h5NlnEAbr4AOmI0uphvGg5k4GDSCbOVvdtQSnwtReJGqqky46i+g3SHJzTXijH/PG523BWL3DvSpmifyUtG+xFCSWRHKp8gMzFgsRN1y03OfzFyduar2RZjrBGEch7rxptGgQnoN126UfyAWtd54fzebLPY2EPJxSb6kd/A7apIutwRqKGBe93yIocHW0TtNvEc5FvRVDJgJGR9kHGmue0JHAK+Yg9XBnPSXdYcDzOqwI7Vu6AvKisvaxU2nLYEJtQvPepR1ivXLatxCeuEHh43dGhpWpuaNqAG3rm/o07g2brGvtbc4u4yar34g0sVKEKgp9VUunOgB/XYF+vJ7w0gE327UvQZl0xeNWvTZtE3f78Wk3qT+QCPtMjJP0XbyqzVfzYBapFF72SxNXXopFoM/1WzJUQdo5xjDZbCL3wVBGEOdtSN/x9fo2KVQ+porkiViHYbn9fTXb1jXf12zHFovZLDuzJPquWiYOuw+m7C9ShGIIufV00l/FYaJtVEVBthMTFwzca+ma3/Gz/aK3okKgFDtwuL259ZwauZrlFDJzzH3PsAcbO6YYMK2CwRrOSfSqHVcqnJUcQJbs6grfo4VjSWKjMLSwmMKKZ+Fif7V6AY6kU4XCEKEkbN2wEK6WhbyclNlejVGoPazBQMKZmSPzuaVtSmZhi9YcOqlxPWJoyGuPA1r65wI+1sweDB9gt6dP2PtjAvG5wtE/h1M6mUUIYlqPXAM9n4znfY5BLvcHG9g3NcB2r5d0qXHJq+d/AW1EmatnlWKCfgkCA6LwZVNvoAIbWf6VEWwYkXtBRe6yFhFV3g8djE5x8Ci9g41AaFrSyG1SKFZOQjz3XSB4Ax77c53H8cAnwdrWKT4U46GGQm9x29lQ34RHNKURWMWXVAm+saC3SSyb8q+BDqdey6KuegFSZGexaDmDAR9Iqaml+igMMRzE9dPbFaKqiZAmN9eErFqbKsLsqwnw5h4EePmBdz50FmqbwbH/CSIi+nOiBAnDZtsq9NGdgmKFMeMidjy5xFUZBjG/whdvHkW4uhF3RWo+4RXeCOK0EKISG0SvsiRh7Q7uPtzEMdHwYk+zYtHQxnEa92rEJ95UGk8dtx+fEV9fafskC+tWajDrdsSHcHsb3LWa7/i34Fzzf0rBY165apXEuodM30OqOTMj5MU6bnbVQlTRwmesTrjq5y0nAfALvMeJWF+aDWTvi8aPwEc0lin4SzJHZ27C4PlU1mVOfFABTjPVs+NFg2QFdmJ57wYi8fFeMbBGFSXsUIbLzjeLMNzHWDy6UF95u7k3RtpXdu94CIqqnBJrjdAYGx1dEoGcYTG6qXdl7Y8ttgE7lXu/dCv/meSLWkJuKl7Ddn4EcbI0Jqw0mPon61owycwZniW5S87MiF9QgmO/RBltdUynA/wHfRxHR1VQAp8YiPb+3imfqKWlzynxasMVrY4QSmuGQRR3BhTHvOCv7N/JtqVwcYNNmaE7kMSJ0bdgvbn+Cp86/26vyySGkTtphYbp4x8TuWH655JBItr8bbKH8TFx0XMQlOz5/EuT6LngDfWu9DVyXU3zktHH17OY413DaxuMB3H8R14nysCRuC9yrLKI2iPKZeJDq3E50uc66qjYRSk8xapRYeLmiy57P/x/q+TNia9hD65k6henLhEFbDS12EMdFnPUwylk6gX4SA0sS7s4gaV4ro5aOs1deoKUYD59zG1I0y5m6VpS1M0SVuMZ+lDUmtHc3YgSKH9Or7QDO03EL3WUgcCc9WUTHZM+HOea5L9ERM6J2SCs11cvwwT8Rtd1++bvG7yh6eP8f7Oh4OH+5YHy70PffLQqXOdrBl9qV4+6vKuTuEsd292R89mZBcwTAmjJwMm/hLBO5c9ikHxYxxq8aY/HQk8kYBTrHur0qrmTs2fusPKhH7u0FTCaMGCKTfoznPr0lFpNTjGx/Hny/i0aSLKCwoilACAxcbnQQx/Ny1/2lcWUbRg4fxCRN2Ye6c7QDtps2ek57DhMv4FRfcvcHiYJVpHBrjEoyCMHhDG7NKY0h2z3p7VdAujBQsWzjnOGoEAncZRKNYXDwkOWSVsIlWV1Z+CNFri9fdgRfx2GA5yhSrei1fiD49fOf58DvWzYKHDo1GhWTUWM95wKAuK4CVDU4YOUyQlBTq062vcATqBW1LAVZ4Xvo0RXWrOdlq1rx77cpXAgnYER7YEKJA1Ze2Us7mpWLBg4TxCk2NW2Rmc1tF2/OTPi+zg3FPq2dOu2uN1oduoGbDZbBQwAu0iokNvKglZNPlesNIHVU09earwVPFDOx9qE7cXCxYsRAdaHMxfs+G2xTFZiFp0GdDF0Av1MkUoPnD7rlpFWCYlryzLli7YQh38H8xFRjhQVERXAAAAAElFTkSuQmCC" alt="PurpleAir">
                    </a>
                </div>
                <nav class="pa_header-navigation">

                </nav>
                <div class="pa_header-icons">
                    <a href="https://www2.purpleair.com/cart" class="pa_header-icons_cart">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.40002 6.5H15.6C19 6.5 19.34 8.09 19.57 10.03L20.47 17.53C20.76 19.99 20 22 16.5 22H7.51003C4.00003 22 3.24002 19.99 3.54002 17.53L4.44003 10.03C4.66003 8.09 5.00002 6.5 8.40002 6.5Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M8 8V4.5C8 3 9 2 10.5 2H13.5C15 2 16 3 16 4.5V8" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M20.41 17.03H8" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                        <span class="cart-count-bubble"><span aria-hidden="true">1</span></span>
                    </a>
                    <div class="pa_header-icons_menu">
                        <button>Menu<span class="menu-icon" id="nav-icon"><span></span><span></span><span></span><span></span></span></button>
                    </div>
                </div>
            </div>
        </div>
    </header>`);
                    const pa_header = $('header#purpleair_header');
                    const header_menu = $(data).find('.header__inline-menu>ul');
                    const pa_megamenu = $(data).find('.header-megamenu');
                    let pa_navIcon = pa_header.find('#nav-icon');

                    head.append(`<link rel="stylesheet" href="${url_to_css}">`)
                    pa_header.find('.pa_header-navigation').append(header_menu);
                    pa_header.append(pa_megamenu);

                    $('.pa_header-icons_menu button').on('click', function () {
                        $(this).toggleClass('open');
                        pa_navIcon.toggleClass('open');
                        pa_megamenu.slideToggle();
                        pa_body.toggleClass('megamenu-opened');
                    })

                    $('details').on('click', function (e) {
                        e.preventDefault();
                    })

                    pa_header.find('.pa_header-navigation > .list-menu > li summary').on('click', function () {
                        $(this).parents('li').siblings().find('ul').hide();
                        $(this).parents('li').siblings().find('details').removeAttr('open');
                        $(this).parents('li').find('details').attr('open', '');
                        $(this).parent().find('ul').toggle();
                    })

                    $(document).on('mouseup', function (e) {
                        var container = pa_header.find('.pa_header-navigation .list-menu > li');

                        if (!container.is(e.target) && container.has(e.target).length === 0) {
                            pa_header.find('.pa_header-navigation .list-menu > li').find('ul').hide();
                        }
                    });

                    let lastScrollTop = 0;
                    $(window).on('scroll', function (event) {
                        let st = $(this).scrollTop();
                        if (st > lastScrollTop && st >= pa_header.outerHeight()) {
                            pa_header.css('top', '-' + pa_header.outerHeight() + 'px');
                        } else {
                            pa_header.css('top', '0');
                        }
                        lastScrollTop = st;
                    });
                }

                if (pa_params.includeFooter) {
                    const footer = $(data).find('footer').attr('id', 'purpleair_footer').removeAttr('class');
                    footer.find(".footer__content-header, .footer__content-top, .footer__content-bottom").wrapAll('<div class="purpleair_footer-inner"></div>');
                    pa_body.append(footer);
                }
            }
        })

    })
})
```
