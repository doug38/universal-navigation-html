// Create the jQuery script element
const jqueryScript = document.createElement('script');
jqueryScript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js';
jqueryScript.defer = true;
// Create the custom.js script element
const customScript = document.createElement('script');
customScript.src = './purple-air-dev.js';
customScript.defer = true;

// purpleair-dev link https://cdn.shopify.com/s/files/1/0550/7707/7224/t/17/assets/custom.js?v=1685486759


// Create the pa_params script element
const paParamsScript = document.createElement('script');
paParamsScript.innerHTML = 'var pa_params = {\n  includeHeader: true,\n  includeFooter: true\n};';

// Append the script elements to the head section of the document
document.head.appendChild(jqueryScript);
document.head.appendChild(customScript);
document.head.appendChild(paParamsScript);
